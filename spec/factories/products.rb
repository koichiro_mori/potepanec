FactoryGirl.define do

  # Spree::Productモデルのテストデータを定義
  factory :product, class: Spree::Product do
    id 1
    name 'Ruby on Rails Tote Test'
    description 'test'
    price 1000
    shipping_category_id 1
  end

end