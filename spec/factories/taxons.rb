FactoryGirl.define do

  # Spree::Taxonモデルのテストデータを定義
  factory :taxon, class: Spree::Taxon do
    id 1
    name 'Categories Test'
  end

end