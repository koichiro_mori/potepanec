require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'Get #show' do
    let(:product) { create(:product) }
    let(:params) { { id: product.id } }
    before:each do
        get :show, params: params
    end

    it 'リクエストは200 OKとなること' do
      expect(response.status).to eq(200)
    end

    it ':showテンプレートを表示すること' do
      expect(response).to render_template :show
    end

  end
end