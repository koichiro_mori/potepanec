module ApplicationHelper

  # ページごとの完全なタイトルを返します。
  def full_title(page_title = '')
    base_title = "Potepan EC"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  # 文字列に含まれる空白をすべて削除する(動作確認済み)
  # def del_space_all(str)
  #   str.gsub(" ", "")
  # end
end
