Spree::Product.class_eval do
  scope :excluding, -> product { where.not(id: product.id) }
  scope :in_the_same_category_of, -> product { where(spree_taxons: { id: product.taxons }).distinct }

end