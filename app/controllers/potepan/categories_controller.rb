class Potepan::CategoriesController < ApplicationController

  def show
    @taxonomies = Spree::Taxonomy.includes(:root)
    @product_count = Spree::Taxon.joins(:products).group(:taxon_id).count(:product_id)
    @disp_type = params[:disp_type] || 'grid'
    @taxon = Spree::Taxon.find_by_id(params[:id])
  end

end