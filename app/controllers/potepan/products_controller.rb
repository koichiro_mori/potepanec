class Potepan::ProductsController < ApplicationController

  def show
    @product = Spree::Product.find_by_id(params[:id])
    @similar_products = Spree::Product.joins(:taxons).excluding(@product).in_the_same_category_of(@product)
  end
end